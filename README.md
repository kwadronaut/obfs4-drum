Running an obfs4 container in float
---

s6-base, a small init, is used to launch obfsvpn. It needs to have some variables:

* RHOST: where traffic should go to, like the IP of where your OpenVPN server
  listens on. If it's on the same host 127.0.0.1:1194 would do.
* LHOST: listen on this IP:PORT 
