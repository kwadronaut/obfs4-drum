FROM debian:bullseye AS build
RUN echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" > /etc/apt/sources.list.d/backports.list
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
git ca-certificates golang-1.18-go build-essential \
&& rm -rf /var/lib/apt/lists/*

# don't need to do bash tricks to keep the layers small, as this is a multi-stage build
ENV PATH=$PATH:/usr/lib/go-1.18/bin/
# update-alternatives
ENV GOPATH=/go
WORKDIR $GOPATH

RUN git clone https://0xacab.org/leap/obfsvpn.git /obfsvpn && cd /obfsvpn/server && go build 
RUN strip /obfsvpn/server/server

# FROM registry.git.autistici.org/ai3/docker/chaperone-base:bullseye
FROM registry.git.autistici.org/ai3/docker/s6-base
RUN apt-get -q update 
COPY --from=build /obfsvpn/server /usr/local/bin/obfsvpn
## COPY chaperone.d/ /etc/chaperone.d
RUN ls -al /usr/local/bin && date && ls -al /usr/local/bin
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
libcap2-bin netcat-openbsd iptables iproute2 \
&& rm -rf /var/lib/apt/lists/*

RUN ls -la /usr/local/bin && ls -al /usr/local/bin/obfsvpn
ENV VPN 127.0.0.1:443
#RUN /usr/local/bin/obfsvpn -add ${LHOST} -vpn ${RHOST} -state test_data -c test_data/obfs4.json
#RUN setcap cap_net_bind_service+ep /usr/local/bin/obfsvpn/server
# RUN setcap cap_net_admin
#RUN setcap cap_net_admin,cap_net_raw,cap_net_bind_service+ep /usr/local/bin/obfsvpn/server
RUN setcap cap_net_admin,cap_net_raw,cap_net_bind_service+ep /usr/local/bin/obfsvpn/server
# COPY --from=build /usr/local/bin/obfsvpn/test_data /obfsvpn/test_data
RUN pwd && ls -al /usr/local/bin/obfsvpn/ && capsh --print
# RUN /usr/local/bin/obfsvpn/server  -add ${LHOST} -vpn ${RHOST} -state test_data -c test_data/obfs4.json
#RUN /usr/local/bin/obfsvpn/server -vpn ${VPN} -state /usr/local/bin/obfsvpn/test_data -v -c /usr/local/bin/obfsvpn/test_data/obfs4.json
# RUN /usr/local/bin/obfsvpn/server -vpn 127.0.0.1:1443  -state /usr/local/bin/obfsvpn/test_data -v -c /usr/local/bin/obfsvpn/test_data/obfs4.json

